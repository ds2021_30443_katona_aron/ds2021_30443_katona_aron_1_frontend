export const environment = {
  production: true,
  apiBaseUrl: "https://ds2021-katona-aron-1-backend.herokuapp.com",
  appTitle: "Energy Platform",
  allowedDomains: ["api.energy.katonaaron.com", "ds2021-katona-aron-1-backend.herokuapp.com"]
};
