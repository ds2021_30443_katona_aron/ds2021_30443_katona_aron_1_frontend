import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SensorDeviceComponent} from './sensor-device.component';

describe('SensorDeviceComponent', () => {
  let component: SensorDeviceComponent;
  let fixture: ComponentFixture<SensorDeviceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SensorDeviceComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorDeviceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
