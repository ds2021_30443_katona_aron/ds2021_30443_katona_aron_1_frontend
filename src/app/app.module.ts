import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {ErrorPageComponent} from './error-page/error-page.component';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {environment} from "../environments/environment";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './header/header.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ClientsComponent} from './clients/clients.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ClientListComponent} from "./clients/client-list/client-list.component";
import {ClientListItemComponent} from "./clients/client-list/client-list-item/client-list-item.component";
import {ApiModule, Configuration} from "@energy-platform/energy-platform-api";
import {ClientTableComponent} from './clients/client-table/client-table.component';
import {NgVarDirective} from './shared/directives/ng-var.directive';
import {ClientEditComponent} from './clients/modals/client-edit/client-edit.component';
import {ClientCreateComponent} from './clients/modals/client-create/client-create.component';
import {DevicesComponent} from './devices/devices.component';
import {DevicesCreateComponent} from './devices/modals/devices-create/devices-create.component';
import {DevicesEditComponent} from './devices/modals/devices-edit/devices-edit.component';
import {DevicesTableComponent} from './devices/devices-table/devices-table.component';
import {DeviceOwnerComponent} from './devices/modals/device-owner/device-owner.component';
import {NgSelectModule} from "@ng-select/ng-select";
import {SensorsComponent} from './sensors/sensors.component';
import {SensorTableComponent} from './sensors/sensor-table/sensor-table.component';
import {SensorEditComponent} from './sensors/modals/sensor-edit/sensor-edit.component';
import {SensorCreateComponent} from './sensors/modals/sensor-create/sensor-create.component';
import {SensorDeviceComponent} from './sensors/modals/sensor-device/sensor-device.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './auth/login/login.component';
import {AppJwtModule} from "./auth/jwt/app-jwt.module";
import {ConsumptionComponent} from './consumption/consumption.component';
import {DeviceConsumptionComponent} from './consumption/device-consumption/device-consumption.component';


@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    HeaderComponent,
    ClientsComponent,
    ClientListComponent,
    ClientListItemComponent,
    ClientTableComponent,
    NgVarDirective,
    ClientEditComponent,
    ClientCreateComponent,
    DevicesComponent,
    DevicesCreateComponent,
    DevicesEditComponent,
    DevicesTableComponent,
    DeviceOwnerComponent,
    SensorsComponent,
    SensorTableComponent,
    SensorEditComponent,
    SensorCreateComponent,
    SensorDeviceComponent,
    HomeComponent,
    LoginComponent,
    ConsumptionComponent,
    DeviceConsumptionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AppJwtModule,
    ApiModule.forRoot(() => new Configuration({
      basePath: environment.apiBaseUrl
    })),
    NgbModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
