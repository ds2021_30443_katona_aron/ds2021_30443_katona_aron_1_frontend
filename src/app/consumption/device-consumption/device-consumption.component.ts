import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {EnergyConsumption, EnergyConsumptionService} from "@energy-platform/energy-platform-api";
import {faCalendar} from '@fortawesome/free-solid-svg-icons';
import {NgbDateAdapter, NgbDateNativeAdapter} from "@ng-bootstrap/ng-bootstrap";

// import * as pluginDataLabels from 'chartjs-plugin-datalabels';

@Component({
  selector: 'app-device-consumption',
  templateUrl: './device-consumption.component.html',
  styleUrls: ['./device-consumption.component.css'],
  providers: [{provide: NgbDateAdapter, useClass: NgbDateNativeAdapter}]
})
export class DeviceConsumptionComponent implements OnInit {

  deviceId: string | null = null;
  day: Date = new Date()
  faCalendar = faCalendar;

  // public barChartData: ChartDataSets[] = [];
  // public barChartLegend = true;
  // public barChartType: ChartType = 'bar';
  // public barChartLabels: Label[];
  // public barChartOptions: ChartOptions = {
  //   responsive: true,
  //   // We use these empty structures as placeholders for dynamic theming.
  //   scales: {xAxes: [{}], yAxes: [{}]},
  //   plugins: {
  //     datalabels: {
  //       anchor: 'end',
  //       align: 'end',
  //     }
  //   }
  // };
  // public barChartPlugins = [];
  public consumptions: EnergyConsumption[] = [];

  constructor(private route: ActivatedRoute,
              private energyConsumptionService: EnergyConsumptionService) {
    // var labels = Array<string>();
    // for (let i = 0; i < 23; i++) {
    //   labels.push(i + "")
    // }
    // this.barChartLabels = labels
    // console.log(labels)
  }

  ngOnInit(): void {
    this.route.params.subscribe(
      (params) => {
        this.deviceId = params.deviceId;

        if (!this.deviceId) {
          return
        }

        this.drawChart();

      });
  }

  private drawChart() {
    if (!this.day || !this.deviceId) {
      return
    }

    this.energyConsumptionService.viewEnergyConsumptionOfDevice(this.deviceId).subscribe(
      consumptions => {
        this.consumptions = consumptions;
      }
    )
  }

}
