import {Component, ElementRef, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {CreateDeviceDto} from "@energy-platform/energy-platform-api/model/createDeviceDto.model";

@Component({
  selector: 'app-devices-create',
  templateUrl: './devices-create.component.html',
  styleUrls: ['./devices-create.component.css']
})
export class DevicesCreateComponent implements OnInit {
  @ViewChild('content') modal!: ElementRef;
  @Input() isOpen = false;
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter()
  @Output() createSubmit: EventEmitter<CreateDeviceDto> = new EventEmitter()
  @Input() loading = false;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter()
  @Input() error: string = "";


  form: FormGroup;

  constructor(private modalService: NgbModal,
              private fb: FormBuilder) {
    this.form = this.fb.group({
      description: ['', Validators.required],
      address: ['', Validators.required],
      maxEnergyConsumption: ['', Validators.required],
      baselineEnergyConsumption: ['', Validators.required]
    });
  }

  get f(): { [p: string]: AbstractControl } {
    return this.form.controls;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isOpen) {
      const isOpen = changes.isOpen.currentValue
      this.toggleModal(isOpen)
    }
  }

  submitPressed() {
    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loadingChange.emit(true)
    this.createSubmit.emit(this.form.value)
  }

  private toggleModal(open: boolean) {
    if (open) {
      this.form.reset();
      this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.isOpenChange.emit(false);
      }, () => {
        this.isOpenChange.emit(false);
      });
    } else {
      this.isOpenChange.emit(false);
      this.modalService.dismissAll();
    }
  }
}
