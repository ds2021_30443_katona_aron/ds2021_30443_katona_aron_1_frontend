import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Device} from "@energy-platform/energy-platform-api/model/device.model";
import {Client, ClientsService, DevicesService} from "@energy-platform/energy-platform-api";
import {Subscription} from "rxjs";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-device-owner',
  templateUrl: './device-owner.component.html',
  styleUrls: ['./device-owner.component.css']
})
export class DeviceOwnerComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('content') modal!: ElementRef;
  @Input() isOpen = false;
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter()
  @Input() device!: Device
  @Output() replace: EventEmitter<{ device: Device, clientId: string }> = new EventEmitter()
  @Output() remove: EventEmitter<Device> = new EventEmitter()
  @Input() loading = false;
  @Output() loadingChange: EventEmitter<boolean> = new EventEmitter()
  @Input() error: string = "";

  clients: Client[] = [];
  clientsSubscription!: Subscription;

  selectedOwnerId!: string

  constructor(private clientsService: ClientsService,
              private deviceService: DevicesService,
              private modalService: NgbModal) {
  }

  ngOnInit(): void {
    this.fetchClients();
  }

  replacePressed() {
    if (!this.selectedOwnerId) {
      this.removePressed()
      return;
    }

    this.loadingChange.emit(true)
    this.replace.emit({device: this.device, clientId: this.selectedOwnerId})
  }

  removePressed() {
    this.loadingChange.emit(true)
    this.remove.emit(this.device)
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isOpen) {
      const isOpen = changes.isOpen.currentValue
      this.toggleModal(isOpen)
    }

    if (changes.device) {
      const device = changes.device.currentValue as Device;
      if (device) {
        this.deviceService.viewOwnerOfDevice(device.id).subscribe(
          owner => {
            this.selectedOwnerId = owner?.id;
          }, error1 => {
            console.log(error1)
          }
        )
      }
    }
  }

  ngOnDestroy(): void {
    if (this.clientsSubscription) {
      this.clientsSubscription.unsubscribe();
    }
  }

  private fetchClients() {
    this.clientsSubscription = this.clientsService.viewAllClients().subscribe(
      clients => {
        this.clients = clients;
      }
    )
  }

  private toggleModal(open: boolean) {
    if (open) {
      this.modalService.open(this.modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        this.isOpenChange.emit(false);
      }, () => {
        this.isOpenChange.emit(false);
      });
    } else {
      this.isOpenChange.emit(false);
      this.modalService.dismissAll();
    }
  }
}
