import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TokenService} from './token.service';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {User, UserRole} from './user.model';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public currentUser: Observable<User | null>;
  private readonly urlLogin = environment.apiBaseUrl + '/login';
  private readonly currentUserSubject: BehaviorSubject<User | null>;

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService,
              private tokenService: TokenService) {
    this.currentUserSubject = new BehaviorSubject<User | null>(this.tokenToUser(tokenService.getToken()));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User | null {
    return this.currentUserSubject.value;
  }

  public isAuthenticated(): boolean {
    return !this.jwtHelper.isTokenExpired(this.tokenService.getToken());
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post<any>(this.urlLogin, {username, password}, {observe: 'response'})
      .pipe(
        map(resp => {
          const token = resp.headers.get('X-Token');
          console.log(token);
          if (token == null) {
            throw throwError("Invalid token")
          }

          const user = this.tokenToUser(token);
          console.log(user);

          if (user == null) {
            throw throwError("Invalid token")
          }

          this.tokenService.setToken(token);
          this.currentUserSubject.next(user);
          return user;
        }),
        // shareReplay(1)
      );
    // this is just the HTTP call,
    // we still need to handle the reception of the token
    // .shareReplay();
  }

  logout(): void {
    this.tokenService.clearToken();
    if (this.currentUserSubject) {
      this.currentUserSubject.next(null);
    }
  }

  private tokenToUser(token: string): User | null {
    const decodedToken = this.jwtHelper.decodeToken(token);
    console.log(decodedToken)
    if (decodedToken == null) {
      return null;
    }

    const user = new User();
    switch (decodedToken.role) {
      case 'ROLE_CLIENT':
        user.role = UserRole.ROLE_CLIENT;
        user.clientId = decodedToken.clientId;
        break;
      case 'ROLE_ADMIN':
        user.role = UserRole.ROLE_ADMIN;
        user.clientId = null;
        break;
      default:
        return null;
    }
    user.username = decodedToken.sub;
    return user;
  }
}
